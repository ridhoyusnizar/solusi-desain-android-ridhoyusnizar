
fun sort(num:IntArray):IntArray{
    var swap = true
    while(swap){
        swap = false
        for(i in 0 until num.size-1){
            if(num[i] > num[i+1]){
                val temp = num[i]
                num[i] = num[i+1]
                num[i+1] = temp
                swap = true

            }
        }
    }
    return num
}
fun hello(name:String = "World"){
    val name = name.toLowerCase()
    println("Hello ${name.capitalize()}")
}

fun isPalindrome(value:String){
    var str = value.replace("\\s".toRegex(),"").toLowerCase()
     var str2 = str.replace(Regex("""[$,.,?]"""),"")
    var reversed = str2.reversed()

    println(str2)
    if(str2.equals(reversed)){
        println("true")
    }else {
        println("false")
    }
}

fun main(){
    val list = sort(intArrayOf(4,9,7,5,8,9,3))
    for(x in list) println("$x")
    hello("budFuDsZm")
    isPalindrome("Borrow or rob?")



}
